/*
Server - 
Gamestate
PlayerID, ip:port
timing

*/


#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define INPUT_YES 'Y'
#define INPUT_NO 'N'


#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <iostream>
#include "network_protocol.h"
#include "timer.h"

#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_PORT "27015"
#define BUFSIZE 512

int xSize = 6;
int ySize = 4;
int points[2] = {};
int goalPos[4] = {};

char* recvbuf = new char[512];

int RecieveMessageFromClient(SOCKET sockfd, sockaddr * server,  ClientToServerMessage* clientMsg)
{
	int sizePoint = sizeof(*server);

	int iResult = recvfrom(sockfd, recvbuf, BUFSIZE, 0, server, &sizePoint);
	if (iResult == SOCKET_ERROR && !WSAEWOULDBLOCK) 
	{
		printf("recieve failed with error: %d\n", WSAGetLastError());
		closesocket(sockfd);
		WSACleanup();
		system("pause");
		return 1;
	}

	DeserializeFromClient(recvbuf, clientMsg);

	return iResult;
}

int SendMessageToClient(SOCKET sockfd, sockaddr* server, ServerToClientMessage* serverMsg)
{
	char sendbuf[BUFSIZE];
	int sizePoint = sizeof(*server);
	SerializeForClient(serverMsg, sendbuf);


	int iResult = sendto(sockfd, sendbuf, BUFSIZE, 0, server, sizePoint);			//sendto
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: \t%d\n", WSAGetLastError());
		//closesocket(clientSocket);
		WSACleanup();
		return 1;
	}

	return iResult;
}

void ServerMessage(ServerToClientMessage* serverMsg)
{
	
	serverMsg->tmpID = '\0';
	serverMsg->gameState.players[0].ID = '0';
	serverMsg->gameState.players[0].x = 2.f;
	serverMsg->gameState.players[0].y = 2.f;
	serverMsg->gameState.players[0].rotationAngle = 1.f;
	serverMsg->gameState.players[1].ID = '0';
	serverMsg->gameState.players[1].x = 0.0f;
	serverMsg->gameState.players[1].y = 0.0f;
	serverMsg->gameState.players[1].rotationAngle = 1.f;
	serverMsg->gameState.players[2].ID = '0';
	serverMsg->gameState.players[2].x = 3.f;
	serverMsg->gameState.players[2].y = 3.f;
	serverMsg->gameState.players[2].rotationAngle = 1.f;
	serverMsg->gameState.players[3].ID = '0';
	serverMsg->gameState.players[3].x = 4.f;
	serverMsg->gameState.players[3].y = 4.f;
	serverMsg->gameState.players[3].rotationAngle = 1.f;
	serverMsg->gameState.goal.x = goalPos[0];
	serverMsg->gameState.goal.y = goalPos[0];
}


int main(void){
	//Initialize timer.h
	initTimerStuff();
	goalPos[0] = 8;
	goalPos[1] = -8;
	goalPos[2] = 15;
	goalPos[3] = -20;

	// Initialize Winsock
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if(iResult != 0){
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}
	// Resolve the server address and port
	addrinfo hints = {0};
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = IPPROTO_UDP;
	hints.ai_flags = AI_PASSIVE;
	addrinfo *result;
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if(iResult != 0){
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}
	
	// Create a SOCKET for connecting to server
	SOCKET ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if(ListenSocket == INVALID_SOCKET){
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}


	//Tillagd senast
	u_long mode = 1;
	iResult = ioctlsocket(ListenSocket, FIONBIO, &mode);
	if (iResult == SOCKET_ERROR)
	{
		printf("Unable to make the socket nonblock with error: ", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	
	// Setup the UDP listening socket
	iResult = bind(ListenSocket, result->ai_addr,(int)result->ai_addrlen);
	if(iResult == SOCKET_ERROR){
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// Receive until the peer shuts down the connection
	int sizePoint = sizeof(*result->ai_addr);
	ClientToServerMessage clientMsg;
	ServerToClientMessage serverMsg;
	ServerMessage(&serverMsg);
	int tmp = 1;

	do {
		iResult = RecieveMessageFromClient(ListenSocket, result->ai_addr, &clientMsg);
		
		if (iResult > 0) {
			//printf("Bytes received: %d\n", iResult);
			//printf("IP: \t\t%s\n", ip);

			sockaddr_in * inadder = (struct sockaddr_in*)result->ai_addr;
			char ip[INET_ADDRSTRLEN];
			inet_ntop(AF_INET, &inadder->sin_addr, ip, INET_ADDRSTRLEN);
			
			if (clientMsg.type == MSG_FROMCLIENT_REQUEST_TO_JOIN)
			{
				//Client request join
				for (int i = 0; i <= MAX_PLAYERS; i++)
				{
					if (serverMsg.gameState.players[i].ID == '0')
					{
						serverMsg.gameState.players[i].ID = '0' + (i + 1);
						serverMsg.type = MSG_FROMSERVER_GAME_STATE;
						serverMsg.tmpID = serverMsg.gameState.players[i].ID;

						printf("Client %d joined\n", serverMsg.gameState.players[i].ID - '0');
						i = MAX_PLAYERS + 1;
					}
					if (i == MAX_PLAYERS)
					{
						printf("Server is full\n");
						serverMsg.type = MSG_FROMSERVER_REQUEST_DENIED;
					}
				}
			}

			else if (clientMsg.type == MSG_FROMCLIENT_DISCONNECT)
			{
				//Disconnected client
				for (int i = 0; i < MAX_PLAYERS; i++)
				{
					if (serverMsg.gameState.players[i].ID == clientMsg.ID)
					{
						points[(clientMsg.ID - '0') - 1] = 0;
						printf("Client %d disconnected\n", serverMsg.gameState.players[i].ID - '0');
						serverMsg.gameState.players[i].ID = '0';
					}
				}
				serverMsg.type = MSG_FROMSERVER_DISCONNECTED;
			}

			else if (clientMsg.type == MSG_FROMCLIENT_INPUT)
			{
				serverMsg.request = clientMsg.request;
				//Input	
				if (clientMsg.upButtonIsDown == INPUT_YES)
				{
					//std::cout << "Up: " << clientMsg.upButtonIsDown << std::endl;

					serverMsg.gameState.players[(clientMsg.ID - '0') - 1].y += 0.01f;
				}
				else if (clientMsg.downButtonIsDown == INPUT_YES)
				{
					//std::cout << "Down: " << clientMsg.downButtonIsDown << std::endl;
					serverMsg.gameState.players[(clientMsg.ID - '0') - 1].y -= 0.01f;
				}
				else if (clientMsg.leftButtonIsDown == INPUT_YES)
				{
					//std::cout << "Left: " << clientMsg.leftButtonIsDown << std::endl;
					serverMsg.gameState.players[(clientMsg.ID - '0') - 1].x -= 0.01f;
				}
				else if (clientMsg.rightButtonIsDown == INPUT_YES)
				{
					//std::cout << "Right: " << clientMsg.rightButtonIsDown << std::endl;
					serverMsg.gameState.players[(clientMsg.ID - '0') - 1].x += 0.01f;
				}

				//Goal hit check
				if (serverMsg.gameState.players[(clientMsg.ID - '0') - 1].x < serverMsg.gameState.goal.x + (xSize / 2) && serverMsg.gameState.players[(clientMsg.ID - '0') - 1].x > serverMsg.gameState.goal.x - (xSize / 2))
				{
					if (serverMsg.gameState.players[(clientMsg.ID - '0') - 1].y > serverMsg.gameState.goal.y - (ySize / 2) && serverMsg.gameState.players[(clientMsg.ID - '0') - 1].y < serverMsg.gameState.goal.y + (ySize / 2))
					{
						//Tr�ff po�ng och repos
						points[(clientMsg.ID - '0') - 1]++;
						serverMsg.gameState.goal.x = goalPos[tmp];	tmp++;
						if (tmp >=  4)
						{
							tmp = 0;
						}
						std::cout << "Player " << clientMsg.ID << " scored!\nPlayer " << clientMsg.ID << " points: " << points[(clientMsg.ID - '0') - 1] << std::endl;
					}
				}
			}
			// Echo the buffer back to the sender.
			int iSendResult = SendMessageToClient(ListenSocket, result->ai_addr, &serverMsg);
			//printf("Bytes sent: \t%d\n", iSendResult);
		}
		else if(iResult == 0){
			printf("Connection closing...\n");

		}
		else if (WSAEWOULDBLOCK) {

		}
		else  {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			WSACleanup();
			return 1;
		}
		serverMsg.gameState.frameCount = elapsedMicroSeconds();
		//sleepyTime();

	} while(true);

	// shutdown the connection since we're done
	iResult = shutdown(ListenSocket, SD_SEND);
	if(iResult == SOCKET_ERROR){
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// cleanup
	
	closesocket(ListenSocket);
	WSACleanup();
	system("pause");
	return 0;
}

