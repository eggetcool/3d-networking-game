#pragma once
#include <WinSock2.h>

struct ServerToClientMessage;
struct ClientToServerMessage;
struct Player;
struct Goal;

//No need for connectSocket in arguments. CLEANUP .77 CAL MOFO

class Networking
{
public:
	void Initialize();
	int Update(bool isRunning);
	void Input(char input);
	void positionGoal(Goal* goal);
	void positionPlayers(Player players[]);
	void positionPredict(float* posX, float* posY, float* rotationAngle);
	void positionCheck(float* posX, float* posY, float* rotationAngle);
	void positionInterpolation(float *posX, float *posY);
	int ShutdownServerConnection(SOCKET connectSocket);
	int retrieveID();


private:
	int RecieveMessageFromServer(SOCKET sockfd, sockaddr* server, ServerToClientMessage* serverMsg);
	int SendMessageToServer(SOCKET sockfd, sockaddr* server, ClientToServerMessage* msg);
	int InitializeServerConnection(SOCKET *connectSocket, addrinfo* result);



	
	SOCKET connectSocket = INVALID_SOCKET;
	addrinfo result;
};