// this is a simple protocol for sending messages between game and server
// include this in client and server
#include <math.h>
#include "vec3.h"



#define MSG_FROMCLIENT_REQUEST_TO_JOIN '1'
#define MSG_FROMCLIENT_DISCONNECT '2'
#define MSG_FROMCLIENT_INPUT '3'

#define MSG_FROMSERVER_GAME_STATE '1'
#define MSG_FROMSERVER_REQUEST_DENIED '2'
#define MSG_FROMSERVER_DISCONNECTED '3'

#define SERVER_PORT "27015"

#define MAX_PLAYERS 4

struct ClientToServerMessage {
	double clientFrameCount;
	int request;
	char type;
	char ID;
	char leftButtonIsDown;
	char rightButtonIsDown;
	char upButtonIsDown;
	char downButtonIsDown;
};

struct Player {
	char ID; // 0+ for valid player ID, -1 for empty slot
	float x;
	float y;
	float rotationAngle;
};

struct Goal {
	float x;
	float y;
};

struct GameState {
	// frameCount is to make sure we don't overwrite client state
	// with older server game state when messages arrive out of order
	double frameCount;
	bool hit;
	Player players[MAX_PLAYERS];
	Goal goal;
};

struct ServerToClientMessage {
	int request;
	char type;
	char tmpID;
	GameState gameState;
};

void SerializeForClient(ServerToClientMessage* msgPacket, char *data)
{
	double *p = (double*)data;
	*p = msgPacket->gameState.frameCount; p++;
	int *s = (int*)p;
	*s = msgPacket->request;	s++;
	char *q = (char*)s;
	*q = msgPacket->type;       q++;
	*q = msgPacket->tmpID;		q++;
	for (int l = 0; l < MAX_PLAYERS; l++)
	{
		*q = msgPacket->gameState.players[l].ID; q++;
	}
	float *r = (float*)q;
	for (int l = 0; l < MAX_PLAYERS; l++)
	{
		*r = msgPacket->gameState.players[l].x; r++;
		*r = msgPacket->gameState.players[l].y; r++;
		*r = msgPacket->gameState.players[l].rotationAngle; r++;
	}
	float *k = (float*)r;
	*k = msgPacket->gameState.goal.x; k++;
	*k = msgPacket->gameState.goal.y; k++;
	bool *j = (bool*)k;
	*j = msgPacket->gameState.hit; j++;
}

void DeserializeFromServer(char *data, ServerToClientMessage* msgPacket)
{
	double *p = (double*)data;
	msgPacket->gameState.frameCount = *p;     p++;
	int *s = (int*)p;
	msgPacket->request = *s;	s++;
	char *q = (char*)s;
	msgPacket->type = *q;		q++;
	msgPacket->tmpID = *q;		q++;
	for (int l = 0; l < MAX_PLAYERS; l++)
	{
		msgPacket->gameState.players[l].ID = *q; q++;
	}
	float *r = (float*)q;
	for (int l = 0; l < MAX_PLAYERS; l++)
	{
		msgPacket->gameState.players[l].x = *r; r++;
		msgPacket->gameState.players[l].y = *r; r++;
		msgPacket->gameState.players[l].rotationAngle = *r; r++;
	}
	float *k = (float*)r;
	msgPacket->gameState.goal.x = *k; k++;
	msgPacket->gameState.goal.y = *k; k++;
	bool *j = (bool*)k;
	msgPacket->gameState.hit = *j; j++;
}

void DeserializeFromClient(char *data, ClientToServerMessage* msgPacket)
{
	double *p = (double*)data;
	msgPacket->clientFrameCount = *p;	p++;
	int *s = (int*)p;
	msgPacket->request = *s;	s++;
	char *q = (char*)s;
	msgPacket->type = *q;       q++;
	msgPacket->ID = *q;   q++;
	msgPacket->upButtonIsDown = *q; q++;
	msgPacket->downButtonIsDown = *q; q++;
	msgPacket->leftButtonIsDown = *q; q++;
	msgPacket->rightButtonIsDown = *q; q++;

}

void SerializeForServer(ClientToServerMessage* msgPacket, char *data)
{
	double *p = (double*)data;
	*p = msgPacket->clientFrameCount;	p++;
	int *s = (int*)p;
	*s = msgPacket->request;	s++;
	char *q = (char*)s;
	*q = msgPacket->type;       q++;
	*q = msgPacket->ID;   q++;
	*q = msgPacket->upButtonIsDown; q++;
	*q = msgPacket->downButtonIsDown; q++;
	*q = msgPacket->leftButtonIsDown; q++;
	*q = msgPacket->rightButtonIsDown; q++;
}









/*

struct ClientToServerMessage {
char type;
unsigned int frameCount;
char leftButtonIsDown;
char rightButtonIsDown;
char upButtonIsDown;
char downButtonIsDown;
};

struct Player {
char ID; // 0+ for valid player ID, -1 for empty slot
float x;
float y;
float angle;
};

struct ServerToClientMessage {
char type;
union {
char requestDeniedReason;
struct {
unsigned int frameCount;
Player players[MAX_PLAYERS];
// ...
} gameState;
};
};

*/