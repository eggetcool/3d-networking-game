/*En Draw funktion ser ut s�h�r:		# = gamla s�ttet.
#glClear();
#glMatrixMode(GL_PROJ...
#LoadIdentity();
#Frustum();
#MatrixMode(MODEL_VIEW);
#Translate();

#PushMatrix();
#Translate
#Rotate			   Old way:					Shaders:
#Scale									glGetAttribLocation
#Begin		/	enableClientState		glEnableVertexAttribArray(__)
#Color		/	glVertexPoint			glVertexAttribArray
#Vertex		/	glColorPoint
.			/	glDrawArray/Element		glDrawArray/ELement
.			/	disable					glDisable
.										glFlush();
#glEnd
#PopMatrix


glFlush();*/


/*
init
//Shaders
//vertexBufferObject/VBO
*/


/*VertexShader
#version 330

in vec3 vertexPosition;
in vec3 vertexPosition2;
*/
#define UNICODE
#define _UNICODE
#define _USE_MATH_DEFINES
#define INPUT_UP '1'
#define INPUT_DOWN '2'
#define INPUT_LEFT '3'
#define INPUT_RIGHT '4'
#define NO_INPUT '0'
#define MAX_PLAYERS 4

//TODO: Servern skickar ID position 0 som 0 '/0', men de andra som 58 '0';		DONE 01/05
//		Servern skickar positionerna r�tt f�rutom f�rsta spelarens x-position; �ndra (clientMsg.ID - '0') -1;	DONE 01/05
//		Framecount i servern buggar, samma i klienten;							DONE 01/04
//		�ndra s� att input f�r�ndrar positionerna i servern;					DONE 01/04
//		�ndra s� att positionerna p� bilarna som finns skickas till clienten;	DONE 01/04
//		Input prediction;														DONE 01/03, EXTRA DONE 01/09
//		Hantera MSG_FROMSERVER_REQUEST_DENIED;
//		B�rja g�ra en heightmap som figurerar som en bana;
//		G�r en for-loop som loopar ut en heightmap ist�llet f�r att skriva manuellt;
//		G�r en random for-loop som s�ger vilket z-v�rde som alla vertiserna ska ha;
//		G�r en smoothing funktion som smoothar ut alla vertiser s� att det inte blir hackigt;
//		Interpolation;															DONE 01/09
//		smoothing av interpolation och input prediction;						DONE 01/09
//		Frame Snapshots fr�n servern var 5e tick;
//		Fixa s� att flera klienter kan styra sina olika bilar, just nu fungerar endast 2 klienter;				DONE 01/06
//		ClientFramecount ska b�rja r�kna ifr�n d�r servern skickade sin framecount;								DONE 01/08
//		ClientFrameCount r�knar alldeles f�r fort;								DONE 01/08 CUZ RETARDS
//		St�da positionReq rensningen;
//		G�r spel, f�rsten till rutan;


//#include "vld.h"
#include <math.h>
#include "Networking.h"
#include <windows.h>
#include <gl/gl.h>
#include "VertexList.h"
#include <gl/GLU.h>
#include "win32_modern_opengl.h"
#include "vec3.h"
#include "mat4.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

static int width = 600;
static int height = 400;
static float cubeAngle = 0.0f;
bool sant = 0;
float carRotation = 0.0f;
int ID;

struct Player {
	char ID;
	float x;
	float y;
	float rotationAngle;
};

struct Goal {
	float x;
	float y;
};

Player players[MAX_PLAYERS];
Goal goal;

float boop = 0;

Networking* networkingClass = new Networking;
static GLuint shaderProgram;
GLuint vbo;
GLuint ebo;
GLuint HMebo;


const char* fshSource = R"blabla(
#version 330

uniform float foobar;
uniform float foobra;
uniform float moobra;

in vec3 color;

out vec4 fragColor;
out float gl_FragDepth;

void main()
{
	fragColor = vec4(color, 1.0);
}
)blabla";


const char* fshSource2 = R"asdasd(
#version 330

uniform float foo;
uniform mat4 poo;

in vec3 vertexPosition;
in vec3 vertexColor;

out vec3 color;

 

void main(){
    color = vertexColor;

	vec3 modifyPos = vec3(vertexPosition.x, vertexPosition.y, vertexPosition.z);

    gl_Position = poo * vec4(modifyPos, 1.0);
       
}
)asdasd";


static void initOpenGL()
{
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(colors) + sizeof(heighmVert) + sizeof(heightmCol), 0, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices), sizeof(colors), colors);

	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(colors), sizeof(heighmVert), heighmVert);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(colors) + sizeof(heighmVert), sizeof(heightmCol), heightmCol);

	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glGenBuffers(1, &HMebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, HMebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(heightmInd), heightmInd, GL_STATIC_DRAW);

	if (fragmentShader)
	{
		glShaderSource(fragmentShader, 1, (const GLbyte**)&fshSource, 0);
		glCompileShader(fragmentShader);

		glShaderSource(vertexShader, 1, (const GLbyte**)&fshSource2, 0);
		glCompileShader(vertexShader);

		if (Win32CheckShaderCompileStatus(fragmentShader))
		{
			shaderProgram = glCreateProgram();
			if (shaderProgram)
			{
				glAttachShader(shaderProgram, fragmentShader);
				glAttachShader(shaderProgram, vertexShader);

				glLinkProgram(shaderProgram);
				Win32CheckProgramLinkStatus(shaderProgram);

				glValidateProgram(shaderProgram);
				Win32CheckProgramValidateStatus(shaderProgram);

				glUseProgram(shaderProgram);
			}
		}
	}
}

void draw() {

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	LARGE_INTEGER Result;
	QueryPerformanceCounter(&Result);
	float r = (float)(Result.LowPart & 0xff) / 255.f;

	glClearColor(.1f, .2f, .3f, 1);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//boop �r 
	boop += 0.001f;
	carRotation += 0.001f;

	glUseProgram(shaderProgram);
	GLuint loc = glGetUniformLocation(shaderProgram, "foobar");
	glUniform1f(loc, cos(boop));
	loc = glGetUniformLocation(shaderProgram, "foobra");
	glUniform1f(loc, cos(boop + 90));
	loc = glGetUniformLocation(shaderProgram, "moobra");
	glUniform1f(loc, cos(boop + 45));
	
	GLint posAttrib = glGetAttribLocation(shaderProgram, "vertexPosition");
	GLint colorAttrib = glGetAttribLocation(shaderProgram, "vertexColor");


	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

	glEnableVertexAttribArray(posAttrib);
	glEnableVertexAttribArray(colorAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vertices)));

	GLint modPos = glGetUniformLocation(shaderProgram, "foo");
	glUniform1f(modPos, cos(boop));

	mat4 m = createFrustumMatrix(-1, 1, -1, 1, 3, 3000);

	
	ID = networkingClass->retrieveID();
	m = lookat(&m, 0.0f, -10.0f, 15.f, players[ID - 1].x, players[ID - 1].y, 0.f, 0.f, 1.f, 0.f);

	//Car1 Matrix shit
	mat4 matrix1 = m;
	translate(&matrix1, players[0].x, players[0].y, 0);
	//rotate(&matrix1, M_PI_2 * carRotation, 0, 1, 1);
	//scale(&matrix1, 1, 1, 1);

	GLuint inMat = glGetUniformLocation(shaderProgram, "poo");
	glUniformMatrix4fv(inMat, 1, GL_FALSE, matrix1.m);
	glDrawElements(GL_TRIANGLES, 90, GL_UNSIGNED_INT, 0);


	//Car2 Matrix shit
	mat4 matrix2 = m;
	translate(&matrix2, players[1].x, players[1].y, 0);
	/*rotate(&matrix2, 2 * M_PI * carRotation, 0, 1, 1);
	scale(&matrix2, 1, 1, 1);*/

	inMat = glGetUniformLocation(shaderProgram, "poo");
	glUniformMatrix4fv(inMat, 1, GL_FALSE, matrix2.m);
	glDrawElements(GL_TRIANGLES, 90, GL_UNSIGNED_INT, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, HMebo);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vertices) + sizeof(colors)));
	glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vertices) + sizeof(colors) + sizeof(heighmVert)));

	mat4 hmMatrix = m;
	translate(&hmMatrix, goal.x, goal.y, 0.0f);
	//rotate(&hmMatrix, -M_PI / 2, 0, 0, 1);

	glUniformMatrix4fv(inMat, 1, GL_FALSE, hmMatrix.m);
	glDrawElements(GL_TRIANGLES, sizeof(heightmInd) / sizeof(heightmInd[0]), GL_UNSIGNED_INT, 0);


	glUseProgram(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	glFlush();
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	LRESULT result = 0;
	switch (uMsg) {
	case WM_CLOSE: {
		OutputDebugString(L"WM_CLOSE");
		PostQuitMessage(0);
	} break;
	case WM_SIZE: {
		OutputDebugString(L"WM_SIZE");
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		PostMessage(hwnd, WM_PAINT, 0, 0);
	}break;
	case WM_PAINT: {
		OutputDebugString(L"WM_PAINT 🖌");
		draw();
		PAINTSTRUCT paint;
		BeginPaint(hwnd, &paint);
		EndPaint(hwnd, &paint);
	} break;
	default: {
		result = DefWindowProc(hwnd, uMsg, wParam, lParam);
	} break;
	}
	return result;
}

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	
	
	WNDCLASS windowClass = { 0 };
	windowClass.style = CS_OWNDC; // CS_HREDRAW | CS_VREDRAW
	windowClass.lpfnWndProc = WindowProc;
	windowClass.hInstance = hInstance;
	windowClass.hCursor = LoadCursor(0, IDC_ARROW);
	windowClass.lpszClassName = L"my window class";

	if (RegisterClass(&windowClass)) {

		HWND hWnd = CreateWindowEx(
			0,
			windowClass.lpszClassName,
			L"︎UU Spelprogrammering 3",
			WS_OVERLAPPEDWINDOW | WS_VISIBLE,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			0, 0, hInstance, 0);

		if (hWnd) {

			HDC hDC = GetDC(hWnd);
			PIXELFORMATDESCRIPTOR pixelFormatDescriptor = { 0 };
			pixelFormatDescriptor.nSize = sizeof(pixelFormatDescriptor);
			pixelFormatDescriptor.nVersion = 1;
			pixelFormatDescriptor.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
			pixelFormatDescriptor.iPixelType = PFD_TYPE_RGBA;
			pixelFormatDescriptor.cColorBits = 32;

			int pixelformat = ChoosePixelFormat(hDC, &pixelFormatDescriptor);
			if (pixelformat != 0) {
				if (SetPixelFormat(hDC, pixelformat, &pixelFormatDescriptor)) {

					HGLRC openGLContext = wglCreateContext(hDC);
					wglMakeCurrent(hDC, openGLContext);

					if (Win32LoadOpenGLFunctions())
					{
						initOpenGL();
						networkingClass->Initialize();


						const char* version = (const char*)glGetString(GL_VERSION);
					}

					int isRunning = 1;
					while (isRunning) {
						MSG msg;
						BOOL hasMessage = PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE);
						

						if (hasMessage) {
							switch (msg.message) {
							case WM_QUIT: {
								OutputDebugString(L"WM_QUIT");
								isRunning = 0;
							} break;
							case WM_KEYDOWN: {
								switch (msg.wParam) {
								case VK_ESCAPE: isRunning = 0; break;
								case VK_LEFT: networkingClass->Input(INPUT_LEFT); break;
								case VK_RIGHT: networkingClass->Input(INPUT_RIGHT); break;
								case VK_UP: networkingClass->Input(INPUT_UP); break;
								case VK_DOWN: networkingClass->Input(INPUT_DOWN); break;
								}
							} break;
							default: {
								networkingClass->Input(NO_INPUT);
								TranslateMessage(&msg);
								DispatchMessage(&msg);
							}
							}
						}

						if (isRunning)
						{
							//Networking code here
							networkingClass->positionGoal(&goal);
							networkingClass->positionPlayers(players);
							networkingClass->positionPredict(&players[networkingClass->retrieveID() - 1].x, 
								&players[networkingClass->retrieveID() - 1].y, 
								&players[networkingClass->retrieveID() - 1].rotationAngle);
							networkingClass->Update(isRunning);
							networkingClass->positionCheck(&players[networkingClass->retrieveID() - 1].x, 
								&players[networkingClass->retrieveID() - 1].y, 
								&players[networkingClass->retrieveID() - 1].rotationAngle);
							draw();
						}
						else
						{
							networkingClass->Update(isRunning);
							delete(networkingClass);
							networkingClass = nullptr;
						}
					}
				}
				else MessageBox(NULL, L"SetPixelFormat() failed.", L"Error", MB_OK);
			}
			else MessageBox(NULL, L"ChoosePixelFormat() failed.", L"Error", MB_OK);
		}
		else MessageBox(NULL, L"CreateWindow() failed.", L"Error", MB_OK);
	}
	else MessageBox(NULL, L"RegisterClass() failed.", L"Error", MB_OK);
	return 0;
}


