//#include "vld.h"
#include <ws2tcpip.h>
#include "Networking.h"
#include <conio.h>
#include <stdio.h>
#include <iostream>
#include "network_protocol.h"
#include "timer.h"

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_PORT "27015"
#define BUFSIZE 512

ClientToServerMessage clientMsg;
ServerToClientMessage serverMsg;
char tmpIDcheck = '\0';
int tmpCount = 0;
int arrayPos = 0;
int iResult;
int reqNum = 0;
float lastposX = 0.0f;
float lastposY = 0.0f;
float positionReq[200] = {};
float positionReqN[200] = {};
double secondsElapsed = 0.0;
GameState prevServState;
GameState currServState;

 
void Networking::Initialize()
{
	int iResult = InitializeServerConnection(&connectSocket, &result);
	serverMsg.tmpID = tmpIDcheck;
	clientMsg.ID = tmpIDcheck;
	clientMsg.type = MSG_FROMCLIENT_REQUEST_TO_JOIN;

	iResult = SendMessageToServer(connectSocket, result.ai_addr, &clientMsg);
	printf("Bytes Sent: %ld\n", iResult);

	iResult = RecieveMessageFromServer(connectSocket, result.ai_addr, &serverMsg);
	clientMsg.ID = serverMsg.tmpID;

	/*if(serverMsg.type == MSG_FROMSERVER_REWUEST_DENIED)
	{
		//Hantera en deny....
	}*/

	if (iResult > 0)
		printf("Bytes received: %d\n", iResult);
	initTimerStuff();
}

int Networking::Update(bool isRunning)
{

	if (serverMsg.tmpID == tmpIDcheck)
	{
		if ((iResult = RecieveMessageFromServer(connectSocket, result.ai_addr, &serverMsg)) > 0 && serverMsg.tmpID != tmpIDcheck)
		{
			clientMsg.ID = serverMsg.tmpID;
			clientMsg.clientFrameCount = serverMsg.gameState.frameCount;
			tmpCount = 0;
		}
		else
		{
			iResult = InitializeServerConnection(&connectSocket, &result);
			clientMsg.type = MSG_FROMCLIENT_REQUEST_TO_JOIN;

			//F�r att kunna joina en server utan att servern �r ig�ng och fortfarande f� x,y
			tmpCount = 0;	
			iResult = SendMessageToServer(connectSocket, result.ai_addr, &clientMsg);
		}

	}

	if (serverMsg.tmpID != tmpIDcheck)
	{
		clientMsg.type = MSG_FROMCLIENT_INPUT;
		iResult = SendMessageToServer(connectSocket, result.ai_addr, &clientMsg);

		iResult = RecieveMessageFromServer(connectSocket, result.ai_addr, &serverMsg);
		prevServState = currServState;
		currServState = serverMsg.gameState;
		secondsElapsed = elapsedMSClient(clientMsg.clientFrameCount);
	}
	if (isRunning == 0)
	{
		ShutdownServerConnection(connectSocket);
		
		return 1;
	}
	
	return 0;
}

void Networking::Input(char input)
{
	switch (input) {
	case '1':	clientMsg.upButtonIsDown = 'Y'; break;
	case '2':	clientMsg.downButtonIsDown = 'Y'; break;
	case '3':	clientMsg.leftButtonIsDown = 'Y'; break;
	case '4':	clientMsg.rightButtonIsDown = 'Y'; break;
	case '0':	{
		clientMsg.upButtonIsDown = 'N';
		clientMsg.downButtonIsDown = 'N';
		clientMsg.leftButtonIsDown = 'N';
		clientMsg.rightButtonIsDown = 'N';
	}; break;
	};

	
}

void Networking::positionGoal(Goal* goal)
{
	goal->x = serverMsg.gameState.goal.x;
	goal->y = serverMsg.gameState.goal.y;
}

void Networking::positionPlayers(Player players[])
{
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		players[i].ID = serverMsg.gameState.players[i].ID;
		if (players[i].ID != clientMsg.ID && players[i].ID != '0')
		{
			players[i].x = serverMsg.gameState.players[i].x;
			players[i].y = serverMsg.gameState.players[i].y;
			players[i].rotationAngle = serverMsg.gameState.players[i].rotationAngle;
		}
	}
}


void Networking::positionPredict(float* posX, float* posY, float* rotationAngle)
{
	if (tmpCount == 0)
	{
		//F�r att kunna joina en server utan att servern �r ig�ng och fortfarande f� x,y
		*posX = serverMsg.gameState.players[(clientMsg.ID - '0') - 1].x;
		*posY = serverMsg.gameState.players[(clientMsg.ID - '0') - 1].y;
		tmpCount = 1;
	}

	if (clientMsg.upButtonIsDown == 'Y')
	{
		*posY += 0.01f;
		positionReq[arrayPos] = *posX; arrayPos++;
		positionReq[arrayPos] = *posY; arrayPos++;
		positionReqN[reqNum] = reqNum;
		clientMsg.request = positionReqN[reqNum]; reqNum++;
	}
	if (clientMsg.downButtonIsDown == 'Y')
	{
		*posY -= 0.01f;
		positionReq[arrayPos] = *posX; arrayPos++;
		positionReq[arrayPos] = *posY; arrayPos++;
		positionReqN[reqNum] = reqNum;
		clientMsg.request = positionReqN[reqNum]; reqNum++;
	}
	if (clientMsg.leftButtonIsDown == 'Y')
	{
		*posX -= 0.01f;
		positionReq[arrayPos] = *posX; arrayPos++;
		positionReq[arrayPos] = *posY; arrayPos++;
		positionReqN[reqNum] = reqNum;
		clientMsg.request = positionReqN[reqNum]; reqNum++;
	}
	if (clientMsg.rightButtonIsDown == 'Y')
	{
		*posX += 0.01f;
		positionReq[arrayPos] = *posX; arrayPos++;
		positionReq[arrayPos] = *posY; arrayPos++;
		positionReqN[reqNum] = reqNum;
		clientMsg.request = positionReqN[reqNum]; reqNum++;
	}
}

//Kollar ifall positionerna som har blivit predictade innan st�mmer �verens med de updates som kommer fr�n servern.
void Networking::positionCheck(float* posX, float* posY, float* rotationAngle)
{
	int tmp = 0;
	for (int i = 0; i < arrayPos; i += 2)
	{
		if (positionReqN[tmp] == serverMsg.request)
		{
			if (positionReq[i] != serverMsg.gameState.players[(clientMsg.ID - '0') - 1].x || positionReq[i + 1] != serverMsg.gameState.players[(clientMsg.ID - '0') - 1].y)
			{
				positionInterpolation(posX, posY);

				for (int j = 0; j < arrayPos; j++)
				{
					positionReq[j] = 0.0f;
					positionReqN[j] = 0;

				}
				arrayPos = 0;
				reqNum = 0;
			}
			else if (positionReq[i] == serverMsg.gameState.players[(clientMsg.ID - '0') - 1].x && positionReq[i + 1] == serverMsg.gameState.players[(clientMsg.ID - '0') - 1].y)
			{
				int tmpN = 0;
				//Ta bort f�rsta x, y pos och ta bort request och flytta resten av listan tv� steg bak�t.
				for (int j = 0; j < arrayPos; j += 2)
				{
					positionReq[j] = positionReq[j + 2];
					positionReq[j + 1] = positionReq[j + 3];
					positionReqN[tmpN] = positionReq[tmpN + 1];
					tmpN++;
				}
				arrayPos -= 2;
				reqNum -= 1;
			}
		}
		tmp++;
	}

	if (arrayPos < 1)
	{
		*posX = serverMsg.gameState.players[(clientMsg.ID - '0') - 1].x;
		*posY = serverMsg.gameState.players[(clientMsg.ID - '0') - 1].y;
	}
}

void Networking::positionInterpolation(float *posX, float *posY)
{
	float tCurrent = secondsElapsed - prevServState.frameCount;
	float tTotal = currServState.frameCount - prevServState.frameCount;
	float t = tTotal / tCurrent;
	*posX = poolerp(prevServState.players[(clientMsg.ID - '0') - 1].x, currServState.players[(clientMsg.ID - '0') - 1].x, t);
	*posY = poolerp(prevServState.players[(clientMsg.ID - '0') - 1].y, currServState.players[(clientMsg.ID - '0') - 1].y, t);
}


int Networking::RecieveMessageFromServer(SOCKET sockfd, sockaddr* server, ServerToClientMessage* serverMsg)
{
	char recvbuf[512];
	int sizePoint = sizeof(*server);

	int iResult = recvfrom(sockfd, recvbuf, BUFSIZE, 0, server, &sizePoint);

	if (WSAGetLastError() == 10054)
	{
		return 0;
	}

	else if (iResult == SOCKET_ERROR && WSAGetLastError() != WSAEWOULDBLOCK)
	{
		int errir = WSAGetLastError();
		printf("recieve failed with error: %d\n", WSAGetLastError());
		closesocket(sockfd);
		WSACleanup();
		system("pause");
		return 1;
	}

	else if (iResult > 0)
		DeserializeFromServer(recvbuf, serverMsg);

	return iResult;
}

int Networking::SendMessageToServer(SOCKET sockfd, sockaddr* server, ClientToServerMessage* clientMsg)
{
	char sendbuf[BUFSIZE];

	SerializeForServer(clientMsg, sendbuf);
	int len = sizeof(*server);

	int iResult = sendto(sockfd, sendbuf, BUFSIZE, 0, server, len);		//sendto
	if (iResult == SOCKET_ERROR)
	{
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(sockfd);
		WSACleanup();
		system("pause");
		return 1;
	}

	return iResult;
}

int Networking::InitializeServerConnection(SOCKET *connectSocket, addrinfo* result)
{

	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	// Resolve the server address and port
	addrinfo hints = { 0 };
	hints.ai_family = AF_UNSPEC; //AF_UNSPEC = IPv4 or IPv6, AF_INET = IPv4, AF_INET6 = IPv6
	hints.ai_socktype = SOCK_DGRAM; // SOCK_STREAM = TCP, SOCK_DGRAM = UDP
	hints.ai_protocol = IPPROTO_UDP; // IPPROTO_TCP = TCP, IPPROTO_UDP = UDP

	addrinfo* tempresult;

	iResult = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &tempresult);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds

	*connectSocket = INVALID_SOCKET;

	for (addrinfo * ptr = tempresult; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		*connectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);

		if (*connectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}
		*result = *ptr;
		break;
	}

	if (*connectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}

	//Make socket nonblocking
	u_long mode = 1;
	iResult = ioctlsocket(*connectSocket, FIONBIO, &mode);
	if (iResult == SOCKET_ERROR)
	{
		printf("Unable to make the socket nonblock with error: ", WSAGetLastError());
		WSACleanup();
		return 1;
	}
}

int Networking::ShutdownServerConnection(SOCKET connectSocket)
{
	clientMsg.type = MSG_FROMCLIENT_DISCONNECT;
	int iResult = SendMessageToServer(connectSocket, result.ai_addr, &clientMsg);
	printf("Bytes Sent: %ld\n", iResult);

	iResult = RecieveMessageFromServer(connectSocket, result.ai_addr, &serverMsg);
	if (iResult > 0)
		printf("Bytes received: %d\n", iResult);


	iResult = shutdown(connectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(connectSocket);
		WSACleanup();
		return 1;
	}
	return 0;
}

int Networking::retrieveID()
{
	return (clientMsg.ID - '0');
}
